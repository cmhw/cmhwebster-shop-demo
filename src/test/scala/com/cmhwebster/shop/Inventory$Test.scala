package com.cmhwebster.shop


import org.scalatest.{BeforeAndAfterEach, FunSuite}

/**
  * Created by webstc1 on 02/06/16.
  */
class Inventory$Test extends FunSuite with BeforeAndAfterEach {


  override def afterEach() {

  }

  test("Inventory price list should contain all SalesProducts") {

    val prices = Inventory.priceList
    val expected: Set[SalesProduct] = Set(Apple, Orange, InvalidProduct)
    val actual = prices.keys.toSet

    assert(actual === expected)

  }

  test("Inventory price list should contain correct price for each SalesProduct") {

    val prices = Inventory.priceList

    val apple = prices(Apple)
    val orange = prices(Orange)
    val invalid = prices(InvalidProduct)

    assert((apple == 0.6) && (orange == 0.25) && (invalid == 0.0))

  }

  test("Should be able to construct valid SalesProduct from String") {
    val apple = SalesProduct("Apple")
    val orange = SalesProduct("Orange")
    val invalid = SalesProduct("Fubar")

    assert((apple === Apple) && (orange === Orange) && (invalid == InvalidProduct))

  }
}
