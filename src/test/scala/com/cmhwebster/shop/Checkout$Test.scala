package com.cmhwebster.shop

import org.scalatest.{BeforeAndAfterEach, FunSuite}
import org.scalatest.Matchers._

/**
  * Created by webstc1 on 02/06/16.
  */
class Checkout$Test extends FunSuite with BeforeAndAfterEach {

  val checkoutNoOffers = Checkout()

  val offers: Map[SalesProduct, Offer] = Map(Apple -> BuyOneGetOneFree, Orange -> ThreeForTwo)
  val checkoutWithOffers = Checkout(offers)

  val checkoutOneOffer = Checkout(Map(Apple -> BuyOneGetOneFree))

  test("Check empty shopping list calculated correctly") {

    val shopping: List[Purchase] = Nil
    val expected = 0.0
    val actual = checkoutNoOffers.calculateTotal(shopping)

    assert(actual === expected)

  }


  test("Check Step 1 list calculated correctly") {

    val shopping: List[Purchase] = List(Purchase(Apple), Purchase(Apple), Purchase(Orange), Purchase(Apple))
    val expected = 2.05
    val actual = checkoutNoOffers.calculateTotal(shopping)

    assert(actual === expected)

  }

  test("Test shopping list calculation with invalid product purchase") {

    val shopping: List[Purchase] = List(Purchase(Apple), Purchase(Apple), Purchase(Orange), Purchase(SalesProduct("Fubar")))
    val expected = 1.45
    val actual = checkoutNoOffers.calculateTotal(shopping)

    assert(actual === expected)

  }

  test("Test shopping list calculation with different quantities") {

    val badPurchase: Purchase = Purchase(SalesProduct("Fubar"), 2)
    val shopping: List[Purchase] = List(Purchase(Apple, 2), Purchase(Apple), Purchase(Orange, 7), badPurchase)
    // 2 x 0.60 + 0.60 + 7*0.25 + 2 * 0.0 =
    val expected = 3.55
    val actual = checkoutNoOffers.calculateTotal(shopping)

    assert(actual === expected)

  }

  test("Test shopping list conversion using Strings and Ints") {

    val stringlyTypedShopping: List[(String, Int)] = List("Apple" -> 1, "Apple" -> 1, "Orange" -> 1, "Fubar" -> 1)
    val expected: List[Purchase] = List(Purchase(Apple), Purchase(Apple), Purchase(Orange), Purchase(SalesProduct("Fubar")))
    val actual = checkoutNoOffers.parseStringIntPairsToPurchases(stringlyTypedShopping)

    actual should contain theSameElementsAs expected

  }

  test("Test shopping list conversion using Strings") {

    val stringlyTypedShopping: List[String] = List("Apple", "Apple", "Orange", "Fubar")
    val expected: List[Purchase] = List(Purchase(Apple), Purchase(Apple), Purchase(Orange), Purchase(SalesProduct("Fubar")))
    val actual = checkoutNoOffers.parseStringsToPurchases(stringlyTypedShopping)

    actual should contain theSameElementsAs expected

  }

  // Step 2 Tests:

  test("Consolidated list of purchases is correct") {

    val shopping: List[Purchase] = List(Purchase(Apple), Purchase(Apple), Purchase(Orange), Purchase(Apple))
    val expected: List[Purchase] = List(Purchase(Apple, 3), Purchase(Orange, 1))

    val actual: List[Purchase] = checkoutNoOffers.consolidatePurchases(shopping)

    actual should contain theSameElementsAs expected
  }

  test("Apply required offers") {
    // 3 for 2 on Oranges, buy 1 get 1 free on Apples
    val shopping: List[Purchase] = List(Purchase(Orange), Purchase(Apple), Purchase(Orange), Purchase(Orange), Purchase(Apple))
    val expected = (1 * 0.6) + (2 * 0.25)

    val actual = checkoutWithOffers.calculateTotal(shopping)

    assert(actual === expected)
  }

  test("No items qualify for offers") {
    // 3 for 2 on Oranges, buy 1 get 1 free on Apples
    val shopping: List[Purchase] = List(Purchase(Orange), Purchase(Apple), Purchase(Orange))
    val expected = (1 * 0.6) + (2 * 0.25)

    val actual = checkoutWithOffers.calculateTotal(shopping)

    assert(actual === expected)
  }

  test("Empty list handled correctly when applying offers") {
    // 3 for 2 on Oranges, buy 1 get 1 free on Apples
    val shopping: List[Purchase] = Nil
    val expected = 0.0

    val actual = checkoutWithOffers.calculateTotal(shopping)

    assert(actual === expected)
  }

  test("Apply required offers with more items than offer quantity") {
    // 3 for 2 on Oranges, buy 1 get 1 free on Apples
    val shopping: List[Purchase] = List( Purchase(Apple,3), Purchase(Orange,4))
    val expected = (2 * 0.6) + (3 * 0.25)

    val actual = checkoutWithOffers.calculateTotal(shopping)

    assert(actual === expected)
  }

  test("Mixed offers: Offer on Apples, but not on Oranges offers") {
    // Buy 1 get 1 free on Apples, no offer on Oranges
    val shopping: List[Purchase] = List(Purchase(Orange, 3), Purchase(Apple,3))
    val expected = (2 * 0.6) + (3 * 0.25)

    val actual = checkoutOneOffer.calculateTotal(shopping)

    assert(actual === expected)
  }
}
