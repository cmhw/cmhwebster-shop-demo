package com.cmhwebster.shop

/**
  * Created by webstc1 on 02/06/16.
  */

// Using type class to avoid stringly-typed products
// Real application would need more flexible approach (DB?)
sealed trait SalesProduct
case object Apple extends SalesProduct
case object Orange extends SalesProduct
case object InvalidProduct extends SalesProduct

object SalesProduct extends SalesProduct {

  def apply(name: String):SalesProduct = {
    name.trim.toUpperCase match {
      case "APPLE" => Apple
      case "ORANGE" => Orange
      case _ => InvalidProduct
    }
  }
}


object Inventory {

  def priceList:Map[SalesProduct, BigDecimal] = {
    // initialise prices for known products
    Map(Apple->0.6, Orange->0.25, InvalidProduct->0.0)
  }

}
