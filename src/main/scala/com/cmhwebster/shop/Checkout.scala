package com.cmhwebster.shop

/**
  * Created by webstc1 on 02/06/16.
  */

case class Purchase(product: SalesProduct, quantity: Int = 1)

sealed trait Offer {
  def applyOffer(pricedPurchase: PricedPurchase): BigDecimal
}

case object BuyOneGetOneFree extends Offer {
  def applyOffer(pricedPurchase: PricedPurchase): BigDecimal = {
    val offerQty: Int = pricedPurchase.purchase.quantity / 2
    val stdQty: Int = pricedPurchase.purchase.quantity % 2
    (offerQty * pricedPurchase.price) + (stdQty * pricedPurchase.price)
  }
}

case object ThreeForTwo extends Offer {
  def applyOffer(pricedPurchase: PricedPurchase): BigDecimal = {
    val offerQty: Int = pricedPurchase.purchase.quantity / 3
    val stdQty: Int = pricedPurchase.purchase.quantity % 3
    (offerQty * 2 * pricedPurchase.price) + (stdQty * pricedPurchase.price)
  }
}


case class PricedPurchase(purchase: Purchase, price: BigDecimal)

case class Checkout(currentOffers: Map[SalesProduct, Offer] = Map.empty) {

  val inventory = Inventory

  def getTotalQuantity(purchases: List[Purchase]): Int = {
    purchases match {
      case Nil => 0
      case _ =>
        purchases.map(_.quantity).reduce(_ + _)
    }
  }

  def consolidatePurchases(purchases: List[Purchase]): List[Purchase] = {
    val groupedPurchases =
      purchases.groupBy(p => p.product)
        .map { case (sp: SalesProduct, purs: List[Purchase]) => Purchase(sp, (getTotalQuantity(purs))) }
    groupedPurchases.toList
  }

  def calculateSubTotal(purchases: List[PricedPurchase]): BigDecimal = {
    if (purchases.isEmpty) 0.0
    else
      purchases.map { pp => pp.purchase.quantity * pp.price }.reduce(_ + _)
  }

  def getStandardPrices(purchases: List[Purchase]): List[PricedPurchase] = {
    purchases.map(p => PricedPurchase(p, inventory.priceList(p.product)))
  }

  def getOfferPrices(purchases: List[Purchase]): List[PricedPurchase] = {
    // need to consolidate purchases so we can apply offers
    val groupedPurchases = consolidatePurchases(purchases)
    // get standard prices first
    val purchasePrices = getStandardPrices(groupedPurchases)
    // now apply offers and return priced purchases (quantity = 1, price = total with offer)
    purchasePrices.map { pp =>
      PricedPurchase(Purchase(pp.purchase.product, 1), currentOffers(pp.purchase.product).applyOffer(pp))
    }
  }

  def applyOffers(purchases: List[Purchase]) = {
    val (offer, noOffer) = purchases.partition(p => currentOffers.contains(p.product))
    val offersCost = calculateSubTotal(getOfferPrices(offer))
    val noOffersCost = calculateSubTotal(getStandardPrices(noOffer))

    offersCost + noOffersCost
  }

  def calculateTotal(purchases: List[Purchase]): BigDecimal = {
    if (purchases.isEmpty) 0.0
    else
      applyOffers(purchases)
  }

  // Allow for String inputs because instructions hint at this
  def parseStringIntPairsToPurchases(purchases: List[(String, Int)]): List[Purchase] = {
    purchases.map { case (s, q) => Purchase(SalesProduct(s), q) }
  }

  def parseStringsToPurchases(purchases: List[String]): List[Purchase] = {
    purchases.map { s => Purchase(SalesProduct(s), 1) }
  }
}
